#!/bin/bash

# Create a new project directory
mkdir hello-react
cd hello-react

# Initialize a new npm project
npm init -y

# Install the necessary dependencies
npm install react react-dom


