export const BORROW_BOOK = 'BORROW_BOOK';
export const RETURN_BOOK = 'RETURN_BOOK';
export const BUY_BOOK = 'BUY_BOOK';

export function borrowBook(bookId) {
  return { type: BORROW_BOOK, payload: bookId };
}

export function returnBook(bookId) {
  return { type: RETURN_BOOK, payload: bookId };
}

export function buyBook(bookId) {
  return { type: BUY_BOOK, payload: bookId };
}
