import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import { connect } from 'react-redux';
import { borrowBook, returnBook, buyBook } from './libraryActions';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    height: '100vh',
  },
  paper: {
    padding: theme.spacing(3),
    maxWidth: 400,
  },
}));

function MyComponent({ borrowBook, returnBook, buyBook }) {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <Typography variant="h5" gutterBottom>
          My Library
        </Typography>
        <Box display="flex" justifyContent="space-between">
          <Button variant="contained" color="primary" onClick={() => borrowBook('book123')}>
            Borrow a Book
          </Button>
          <Button variant="contained" color="primary" onClick={() => returnBook('book123')}>
            Return a Book
          </Button>
          <Button variant="contained" color="primary" onClick={() => buyBook('book123')}>
            Buy a Book
          </Button>
        </Box>
      </Paper>
    </div>
  );
}

const mapDispatchToProps = {
  borrowBook,
  returnBook,
  buyBook,
};

export default connect(null, mapDispatchToProps)(MyComponent);
