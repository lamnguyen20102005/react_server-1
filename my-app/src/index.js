// index.js

import React from 'react';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import MyComponent from './MyComponent';
import libraryReducer from './libraryReducer';

const store = createStore(libraryReducer);

function App() {
  return (
    <Provider store={store}>
      <MyComponent />
    </Provider>
  );
}

export default App;
