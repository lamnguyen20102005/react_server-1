import React from 'react';
import { connect } from 'react-redux';
import Button from '@material-ui/core/Button';

function App() {
  return (
    <div>
      <h1>Welcome to my library</h1>
      <Button variant="contained" color="primary">
        Borrow a Book
      </Button>
      <Button variant="contained" color="primary">
        Return a Book
      </Button>
      <Button variant="contained" color="primary">
        Buy a Book
      </Button>
    </div>
  );
}

export default App;
