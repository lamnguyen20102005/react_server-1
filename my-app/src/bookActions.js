const BORROW_BOOK = 'BORROW_BOOK';
const RETURN_BOOK = 'RETURN_BOOK';
const BUY_BOOK = 'BUY_BOOK';

function borrowBook(bookId) {
  return { type: BORROW_BOOK, payload: bookId };
}

function returnBook(bookId) {
  return { type: RETURN_BOOK, payload: bookId };
}

function buyBook(bookId) {
  return { type: BUY_BOOK, payload: bookId };
}
