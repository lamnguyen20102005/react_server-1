const BORROW_BOOK = 'BORROW_BOOK';
const RETURN_BOOK = 'RETURN_BOOK';
const BUY_BOOK = 'BUY_BOOK';
// libraryReducer.js

const initialState = {
    borrowedBooks: [],
    ownedBooks: [],
  };
  
  export default function libraryReducer(state = initialState, action) {
    switch (action.type) {
      case BORROW_BOOK:
        return {
          ...state,
          borrowedBooks: [...state.borrowedBooks, action.payload],
        };
      case RETURN_BOOK:
        return {
          ...state,
          borrowedBooks: state.borrowedBooks.filter(
            (bookId) => bookId !== action.payload
          ),
        };
      case BUY_BOOK:
        return {
          ...state,
          ownedBooks: [...state.ownedBooks, action.payload],
        };
      default:
        return state;
    }
  }
  