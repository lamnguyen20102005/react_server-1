import { BORROW_BOOK, RETURN_BOOK, BUY_BOOK } from './constants';

function libraryReducer(state = initialState, action) {
  switch (action.type) {
    case BORROW_BOOK:
      // handle borrow book action
      break;
    case RETURN_BOOK:
      // handle return book action
      break;
    case BUY_BOOK:
      // handle buy book action
      break;
    default:
      return state;
  }
}
