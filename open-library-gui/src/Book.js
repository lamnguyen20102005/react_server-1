import React from "react";
import Book from "./Book";

const SingleBook = () => {
  const bookData = {
    title: "To Kill a Mockingbird",
    authors: [{ name: "Harper Lee" }],
    cover: {
      medium:
        "http://covers.openlibrary.org/b/id/237463-S.jpg",
      large:
        "http://covers.openlibrary.org/b/id/237463-L.jpg"
    }
  };

  return <Book data={bookData} />;
};

export default SingleBook;
