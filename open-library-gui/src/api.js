import axios from "axios";

const BASE_URL = "http://openlibrary.org/";

export const searchBooks = async (query) => {
  const response = await axios.get(`${BASE_URL}search.json?q=${query}`);
  return response.data.docs;
};

export const getBookDetails = async (key) => {
  const response = await axios.get(`${BASE_URL}api/books?bibkeys=${key}&jscmd=details&format=json`);
  return response.data[key];
};
