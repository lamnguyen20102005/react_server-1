import React, { useState } from 'react';
import axios from 'axios';

function OpenLibrary() {
  const [searchTerm, setSearchTerm] = useState('');
  const [books, setBooks] = useState([]);

  const searchBooks = async () => {
    const response = await axios.get(
      `http://openlibrary.org/search.json?q=${searchTerm}`
    );
    setBooks(response.data.docs);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    searchBooks();
  };

  const handleChange = (event) => {
    setSearchTerm(event.target.value);
  };

  return (
    <div>
      <h1>Open Library Search</h1>
      <form onSubmit={handleSubmit}>
        <label>
          Search:
          <input type="text" value={searchTerm} onChange={handleChange} />
        </label>
        <button type="submit">Search</button>
      </form>
      <ul>
        {books.map((book) => (
          <li key={book.key}>
            <strong>{book.title}</strong> by {book.author_name}
          </li>
        ))}
      </ul>
    </div>
  );
}

export default OpenLibrary;
