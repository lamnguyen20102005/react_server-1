import React, { useState, useEffect } from "react";
import axios from "axios";

const BookList = () => {
  const [books, setBooks] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      const result = await axios(
        "https://openlibrary.org/api/books?bibkeys=ISBN:9780545010221&format=json"
      );

      setBooks(result.data);
    };

    fetchData();
  }, []);

  return (
    <div>
      {Object.values(books).map((book, i) => (
        <Book key={i} data={book} />
      ))}
    </div>
  );
};

export default BookList;
